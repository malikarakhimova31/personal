import React, { useState } from 'react'
import { Box, Container, Typography } from '@mui/material'
import { useFormik } from 'formik'
import { useRouter } from 'next/router'
import OrderProducts from '../components/OrderProducts/OrderProducts'
// import DeliveryType from '../components/DeliveryType/DeliveryType'
import PaymentType from '../components/PaymentType/PaymentType'
import DeliveryAddress from '../components/DeliveryAddress/DeliveryAddress'
import PersonalData from '../components/PersonalData/PersonalData'
import CheckoutInfo from '../components/CheckoutInfo/CheckoutInfo'
import { useDispatch, useSelector } from 'react-redux'
import { cartTotalPriceSelector } from '../store/selectors/cartSelectors'
import axios from '../utils/axios'
import SEO from '../components/seo'
import {
  generatePaymentUniqueId,
  generateUniqueId,
} from '../utils/generateUniqueId'
import { getPaymentSystemId } from '../utils/getPaymentSystemId'
import { toggleOrderNotification } from '../store/actions/defaultActions/defaultActions'
import { menu } from '.././mock/menu'
import { toast } from 'react-toastify'
import { clearCartAction } from '../store/actions/cartActions/cartActions'

// paymentSystemId: 1 cash
// paymentSystemId: 2 click
// paymentSystemId: 4 payme

export default function Checkout() {
  const { cartItems } = useSelector((state) => state.cart)
  const totalPrice = useSelector((state) => cartTotalPriceSelector(state))
  const [loading, setLoading] = useState(false)
  const { locale, push } = useRouter()
  const dispatch = useDispatch()

  const formik = useFormik({
    initialValues: {
      name: '',
      phone: '',
      deliveryType: 'deliver',
      paymentType: 'payme',
      address: '',
      branchId: 0,
      geoLocation: '',
      deliveryDetails: null,
    },
    validate: (values) => {
      const errors = {}
      if (!values.name) {
        errors.name = 'Required'
      }
      if (!values.phone) {
        errors.phone = 'Required'
      } else if (values.phone.split(' ').join('').length < 13) {
        errors.phone = 'Phone is invalid'
      }
      return errors
    },
    onSubmit: (values) => {
      setLoading(true)
      orderCreate(values)
    },
    validateOnBlur: false,
    validateOnChange: false,
  })

  function orderCreate(values) {
    const phoneNumber = values.phone.split(' ').join('').substring(1)
    const uniqueID = generateUniqueId(phoneNumber)
    const data = {
      additional: {
        deliveryAmount: values.deliveryDetails?.deliveryPrice
          .toFixed(2)
          .toString(),
        deliverySum: values.deliveryDetails?.deliveryPrice,
        deliveryTime: values.deliveryDetails?.deliveryTime,
      },
      uniqueID,
      // branchId: values.branchId,
      branchId: 53,
      client: {
        title: values.name,
        phoneNumber,
      },
      sourceType: 8,
      orderType: 2,
      positions: cartItems.map((item) => ({
        menuItemId: item.id,
        count: item.quantity,
        price: item.price.salePrice.toFixed(2).toString(),
        title: item.title,
      })),
      outsideType: 'WEBSITE',
      totalAmount: totalPrice,
      paid: 0,
      grandTotal: totalPrice + parseInt(values.deliveryDetails?.deliveryPrice), // + deliveryPrice
      comment: '',
      deliveryAddress: {
        title: values.address,
        geoLocation: values.geoLocation,
      },
      payments: [
        {
          uniqueid: generatePaymentUniqueId(12),
          paymentSystemId: getPaymentSystemId(values.paymentType),
          amount: totalPrice,
          phoneNumber,
        },
      ],
    }
    console.log('data => ', data)

    axios
      .put('/order/update', data)
      .then((res) => {
        console.log('orderCreate res => ', res)
        if (res.errorMessage) {
          toast.error('Произошла ошибка, попробуйте позже')
          return
        }
        dispatch(toggleOrderNotification())
        dispatch(clearCartAction())
        push('/')
      })
      .catch((err) => {
        console.error(err)
        toast.error('Произошла ошибка, попробуйте позже')
      })
      .finally(() => setLoading(false))
  }

  return (
    <>
      <SEO title={menu[locale].setOrder} />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <Typography variant={`h1`} color={`black`} className='title'>
            {menu[locale].setOrder}
          </Typography>
          <form onSubmit={formik.handleSubmit} className='checkoutFlex'>
            <Box id='boxFullWidthBig' mr={2}>
              <PersonalData formik={formik} />
              <OrderProducts />
              {/* <DeliveryType formik={formik} /> */}
              <DeliveryAddress formik={formik} />
              <PaymentType formik={formik} />
            </Box>
            <Box id='boxFullWidthSml'>
              <CheckoutInfo
                deliveryPrice={formik.values.deliveryDetails?.deliveryPrice}
                loading={loading}
              />
            </Box>
          </form>
        </Container>
      </Box>
    </>
  )
}
