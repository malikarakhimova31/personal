import React from 'react'
import BranchDesc from '../../components/BranchDesc/BranchDesc'
import { Box, Container } from '@mui/material'
import { fetchMultipleUrls } from '../../utils/fetchMultipleUrls'
import SEO from '../../components/seo'

export default function branchDescription({ branchItem }) {
  return (
    <>
      <SEO title='Филиал' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <BranchDesc branchItem={branchItem} />
        </Container>
      </Box>
    </>
  )
}

export async function getServerSideProps(ctx) {
  const { query } = ctx
  const [branchItem] = await fetchMultipleUrls([`branch/${query.id}`])
  // console.log(branchItem);
  return {
    props: {
      branchItem,
    },
  }
}
