import React from 'react'
import BranchMain from '../../components/BranchMainPage/BranchMain'
import SEO from '../../components/seo'
import { fetchMultipleUrls } from '../../utils/fetchMultipleUrls'

function branches({ branches }) {
  return (
    <>
      <SEO title='Филиал' />
      <BranchMain branches={branches} />
    </>
  )
}

export default branches

export async function getServerSideProps() {
  const [branches] = await fetchMultipleUrls(['/branch/bycompany/1/all'])

  return {
    props: {
      branches,
    },
  }
}
