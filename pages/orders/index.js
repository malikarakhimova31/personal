import React from 'react'
import { Box, Container, Typography } from '@mui/material'
import OrderMain from '../../components/OrderMainPage/OrderMain'
import SEO from '../../components/seo'
import { fetchMultipleUrls } from '../../utils/fetchMultipleUrls'
import nookies, { parseCookies } from 'nookies'

export default function Orders({ orders }) {
  console.log('ORDERS', orders)
  return (
    <>
      <SEO title='Заказы' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <OrderMain list={orders.orders} />
        </Container>
      </Box>
    </>
  )
}

export async function getServerSideProps(ctx) {
  const cookie = nookies.get(ctx)

  const urls = [`/order/client/${cookie.client_id}`]

  const [orders] = await fetchMultipleUrls([
    `/order/client/${cookie.client_id}`,
  ])
  return {
    props: {
      orders,
    },
  }
}
