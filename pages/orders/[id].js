import React from 'react'
import { Box, Container, Typography } from '@mui/material'
import MyOrders from '../../components/MyOrders.jsx/MyOrders'
import SEO from '../../components/seo'
import { fetchMultipleUrls } from '../../utils/fetchMultipleUrls'

export default function OrderItems({ orderItem }) {
  console.log('ORDERITEM', orderItem)
  return (
    <>
      <SEO title='Товары для заказа' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <MyOrders orderItem={orderItem} />
        </Container>
      </Box>
    </>
  )
}

export async function getServerSideProps(ctx) {
  const { query } = ctx

  const [orderItem] = await fetchMultipleUrls([`/order/${query.id}`])

  return {
    props: {
      orderItem,
    },
  }
}
