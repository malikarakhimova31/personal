import React from 'react'
import { Box, Container } from '@mui/material'
import VacancyDesc from '../../components/VacancyDesc/VacancyDesc'
import SEO from '../../components/seo'
import { vacancies } from '../../mock/vacancies'

export default function VacancySingle({ vacancy }) {
  return (
    <>
      <SEO title='Описание вакансии' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <VacancyDesc data={vacancy} />
        </Container>
      </Box>
    </>
  )
}

export async function getServerSideProps({ query }) {
  const vacancy = vacancies.find((item) => item.id == query.id)
  return {
    props: {
      vacancy: vacancy || null,
    },
  }
}
