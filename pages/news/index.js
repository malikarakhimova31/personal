import React from 'react'
import { Box, Container } from '@mui/material'
import BlogItems from '../../components/BlogItems/BlogItems'
import SEO from '../../components/seo'
import { news } from '../../mock/news'

export default function News() {
  return (
    <>
      <SEO title='Новости' />
      <Box id='blogRoot'>
        <Container maxWidth='xl'>
          <Box display='flex' id='verticalMargin'>
            <Box width='100%'>
              <BlogItems data={news} />
            </Box>
          </Box>
        </Container>
      </Box>
    </>
  )
}
