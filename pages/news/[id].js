import React from 'react'
import { Box, Container } from '@mui/material'
import BlogDesc from '../../components/BlogDesc/BlogDesc'
import SEO from '../../components/seo'
import { news } from '../../mock/news'

export default function NewSingle({ latestNews }) {
  return (
    <>
      <SEO title={latestNews.title} />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <BlogDesc data={latestNews} />
        </Container>
      </Box>
    </>
  )
}

export async function getServerSideProps({ query }) {
  const data = news.find((item) => item.id == query.id)
  return {
    props: {
      latestNews: data || null,
    },
  }
}
