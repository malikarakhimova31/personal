export const vacancies = [
  {
    id: 0,
    title: 'Повар',
    requirements: [
      {
        title: 'Квалификационные требования:',
        infos: [
          {
            title: 'Пол: Мужской',
          },
          {
            title: 'Возраст: 20 - 35 лет',
          },
          {
            title: 'Требуемый опыт работы: без опыта работы',
          },
          {
            title: 'Образование: Средне-специальное',
          },
        ],
      },
      {
        title: 'Обязанности:',
        infos: [
          {
            title: 'Работа на кухне, приготовление блюд.',
          },
          {
            title: 'Поддержание чистоты на рабочем месте.',
          },
        ],
      },
      {
        title: 'Условия работы:',
        infos: [
          {
            title: 'Сменная работа',
          },
          {
            title: 'Обучение в процессе работы',
          },
          {
            title: 'Карьерный рост',
          },
          {
            title: 'Питание и форма предоставляется работодателем',
          },
          {
            title: 'Достойная и своевременная оплата труда',
          },
        ],
      },
    ],
  },
  {
    id: 1,
    title: 'Курьер',
    requirements: [
      {
        title: 'Квалификационные требования:',
        infos: [
          {
            title: 'Пол: Мужской',
          },
          {
            title: 'Возраст: 20 - 35 лет',
          },
          {
            title: 'Требуемый опыт работы: от 1 года',
          },
          {
            title: 'Знание языков: узбекский, русский – свободное владение',
          },
          {
            title: 'Образование: Средне-специальное',
          },
          {
            title: 'Наличие собственного авто (Matiz/Spark)',
          },
          {
            title: 'Водительские права категории B/BC',
          },
          {
            title: 'Водительский стаж от 3 лет',
          },
          {
            title: 'Высокая степень ответственности, пунктуальность',
          },
        ],
      },
      {
        title: 'Обязанности:',
        infos: [
          {
            title: 'Доставка продукции.',
          },
          {
            title: 'Своевременная сдача отчетов и денег за прошедшие заказы.',
          },
        ],
      },
      {
        title: 'Условия работы:',
        infos: [
          {
            title: 'Постоянная работа, без возможности совмещения',
          },
          {
            title: 'Питание и форма предоставляется работодателем.',
          },
          {
            title: 'Достойная и своевременная оплата труда',
          },
        ],
      },
    ],
  },
  {
    id: 3,
    title: 'Официант',
    requirements: [
      {
        title: 'Квалификационные требования:',
        infos: [
          {
            title: 'Пол: Мужской/Женский',
          },
          {
            title: 'Возраст: 20 - 30 лет',
          },
          {
            title: 'Требуемый опыт работы: без опыта работы',
          },
          {
            title: 'Знание языков: узбекский, русский – свободное владение',
          },
          {
            title: 'Образование: Средне-специальное',
          },
          {
            title: 'Знание компьютера: пользователь',
          },
          {
            title: 'Локация: Мукимий, Алгоритм, Бешкайрогоч, Софийский',
          },
        ],
      },
      {
        title: 'Обязанности:',
        infos: [
          {
            title:
              'Обслуживание гостей, консультирование гостей по ассортименту.',
          },
          {
            title: 'Поддержание и контроль чистоты.',
          },
        ],
      },
      {
        title: 'Условия работы:',
        infos: [
          {
            title: 'Сменная работа',
          },
          {
            title: 'Обучение в процессе работы',
          },
          {
            title: 'Питание и форма предоставляется работодателем',
          },
          {
            title: 'Достойная и своевременная оплата труда',
          },
        ],
      },
    ],
  },
]
