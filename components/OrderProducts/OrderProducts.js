import { IconButton, Typography } from '@mui/material'
import { Box } from '@mui/system'
import Image from 'next/image'
import React from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import { numberToPrice } from '../../utils/numberToPrice'
import CardWrapper from '../CardWrapper/CardWrapper'
import Counter from '../Counter/Counter'
import { TrashIcon } from '../Icons'
import cls from './OrderProducts.module.scss'
import { menu } from '../../mock/menu'
import { useRouter } from 'next/router'

export default function OrderProducts() {
  const { locale } = useRouter()
  const { cartItems } = useSelector((state) => state.cart, shallowEqual)
  return (
    <CardWrapper mt={2} id='orderProducts'>
      <Box display='flex' justifyContent='space-between'>
        <Typography variant={`h1`} color={`black`}>
          {menu[locale].yourOrder}
        </Typography>
        {/* <IconButton onClick={() => console.log('click')}>
          <TrashIcon />
        </IconButton> */}
      </Box>
      <Box mt={4}>
        {cartItems.map((item) => (
          <div key={item.id} className={cls.orderItem}>
            <div className={cls.itemInfo}>
              <Box
              // position='relative'
              // width={96}
              // height={96}
              // bgcolor='secondary.main'
              >
                <Image
                  loader={() => `${process.env.CDN}/${item.deliveryImage}`}
                  src={`${process.env.CDN}/${item.deliveryImage}`}
                  alt={item.title}
                  width={96}
                  height={96}
                />
              </Box>
              <div className={cls.itemName}>
                <Typography variant='h4' sx={{ marginBottom: 1 }}>
                  {item.title}
                </Typography>
                <Typography
                  variant='body4'
                  sx={{ maxWidth: '178px', height: 40, overflow: 'hidden' }}
                  className={cls.itemDescription}
                >
                  {item.description}
                </Typography>
              </div>
            </div>
            <div className={cls.priceInfo}>
              <div style={{ textAlign: 'right' }}>
                <Typography variant={`h8`} sx={{ whiteSpace: 'nowrap' }}>
                  {numberToPrice(item.price.salePrice * item.quantity)}
                </Typography>
                <Counter data={item} />
              </div>
            </div>
          </div>
        ))}
      </Box>
    </CardWrapper>
  )
}
