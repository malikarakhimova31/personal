import React, { useState, useEffect, createRef } from 'react'
import { Box, Container } from '@mui/material'
import cls from './Menu.module.scss'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import { ArrowLeft, ArrowRight } from './SlickArrows'
import ReactSlick from 'react-slick'
import { Link } from 'react-scroll'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { cartTotalPriceSelector } from '../../store/selectors/cartSelectors'
import { openCartDrawer } from '../../store/actions/drawerActions/drawerActions'
import { numberToPrice } from '../../utils/numberToPrice'

function useWindowSize() {
  const [size, setSize] = useState([window.innerWidth])
  return size
}

function Menu({ menu }) {
  const [isAnimation, setAnimation] = useState(false)
  const [activeLink, setActiveLink] = useState('')
  const [categoriesRef] = useState(menu?.map((_, index) => (_ = createRef())))
  const dispatch = useDispatch()

  const [windowWidth] = useWindowSize()
  let MenuActive = true
  if (windowWidth > 1366) {
    MenuActive = false
  } else if (windowWidth > 960 && windowWidth < 1366) {
    MenuActive = true
  } else if (windowWidth > 0 && windowWidth < 960) {
    MenuActive = false
  }
  const [DropdownActive, setDropdownActive] = useState(false)

  const totalPrice = useSelector(
    (state) => cartTotalPriceSelector(state),
    shallowEqual
  )

  const scrollToCategory = (index) => {
    categoriesRef[index]?.current?.scrollIntoView({
      behavior: 'auto',
      block: 'center',
      inline: 'center',
    })
  }

  const handleOpenDrawer = () => {
    dispatch(openCartDrawer())
  }

  useEffect(() => {
    const handleScroll = () => {
      var someDiv = document?.getElementById('slider-category')
      var distanceToTop = someDiv?.getBoundingClientRect()?.top

      if (distanceToTop < -72) {
        setAnimation(true)
      } else {
        setAnimation(false)
      }
    }
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  return (
    <div className={cls.root}>
      <Container maxWidth='xl'>
        <div
          style={{
            transform: isAnimation ? 'translateY(0)' : 'translateY(-100px)',
            height: isAnimation ? '80px' : '0',
          }}
          className={`${cls.animation}`}
        >
          <Container maxWidth='xl'>
            {menu && menu.length > 0 ? (
              <div
                className={cls.menuList}
                style={{
                  height: 80,
                  display: 'flex',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                  justifyContent: 'space-between',
                }}
              >
                <ReactSlick
                  {...{
                    focusOnSelect: true,
                    dots: false,
                    infinite: false,
                    speed: 500,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    variableWidth: true,
                    nextArrow: <ArrowRight styles={cls.nextButton} />,
                    prevArrow: <ArrowLeft styles={cls.prevButton} />,
                    responsive: [
                      {
                        breakpoint: 800,
                        settings: {
                          slidesToShow: 4,
                          slidesToScroll: 2,
                        },
                      },
                      {
                        breakpoint: 600,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                    ],
                  }}
                  className={`${cls.items}`}
                >
                  {menu.map((item, index) => (
                    <li
                      ref={categoriesRef[index]}
                      key={item.id}
                      className={cls.listItem}
                    >
                      <Link
                        activeClass={cls.active}
                        to={item.id.toString()}
                        spy={true}
                        offset={-80}
                        onSetActive={() => scrollToCategory(index)}
                      >
                        {item.title}
                      </Link>
                    </li>
                  ))}
                </ReactSlick>
                {windowWidth > 576 && (
                  <Box className={cls.shopIconWrapper} id='basketIcon'>
                    <button
                      className={`${cls.shop} ${totalPrice ? cls.total : ''}`}
                      onClick={handleOpenDrawer}
                      id='cartButton'
                    >
                      <div className={cls.shopping}>
                        {totalPrice ? (
                          <Box
                            maxWidth='100%'
                            display='flex'
                            alignItems='center'
                            justifyContent='space-between'
                            flexDirection='row-reverse'
                          >
                            <p className={cls.shopText}>
                              {numberToPrice(totalPrice)}
                            </p>
                            <img
                              src='/images/svg/basketWhite.svg'
                              width={24}
                              height={24}
                              alt='cart'
                            />
                          </Box>
                        ) : (
                          <Box
                            maxWidth='100%'
                            display='flex'
                            alignItems='center'
                            justifyContent='space-between'
                            flexDirection='row-reverse'
                          >
                            <p className={cls.shopText}>Корзина</p>
                            <img
                              src='/images/svg/basket.svg'
                              width={24}
                              height={24}
                              alt='cart'
                            />
                          </Box>
                        )}
                      </div>
                    </button>
                  </Box>
                )}
              </div>
            ) : (
              ''
            )}
          </Container>
        </div>

        {/* static menu */}
        <div className={cls.menuListTop} id='slider-category'>
          {!MenuActive && (
            <ul className={cls.list}>
              {menu.map((item) => (
                <li key={item.id} className={cls.listItem}>
                  <AnchorLink
                    offset={80}
                    href={`#${item.id}`}
                    style={{ whiteSpace: 'nowrap' }}
                    onClick={() => {
                      if (activeLink.includes(item.id)) {
                        setActiveLink('')
                      } else {
                        setActiveLink(String(item.id))
                      }
                    }}
                  >
                    {item.title}
                  </AnchorLink>
                </li>
              ))}
            </ul>
          )}

          {MenuActive && (
            <ul className={cls.list}>
              {menu.slice(0, 7).map((item) => (
                <li key={item.id} className={cls.listItem}>
                  <AnchorLink
                    offset={80}
                    href={`#${item.id}`}
                    style={{ whiteSpace: 'nowrap' }}
                    onClick={() => {
                      if (activeLink.includes(item.id)) {
                        setActiveLink('')
                      } else {
                        setActiveLink(String(item.id))
                      }
                    }}
                  >
                    {item.title}
                  </AnchorLink>
                </li>
              ))}

              <div
                className={cls.listItemDropdownLink}
                onMouseEnter={() => setDropdownActive(true)}
                onMouseLeave={() => setDropdownActive(false)}
              >
                <div className={cls.listItem}>
                  <a>
                    Ещё
                    <svg
                      width='12'
                      height='8'
                      viewBox='0 0 12 8'
                      fill='none'
                      xmlns='http://www.w3.org/2000/svg'
                    >
                      <path
                        d='M1.41 7.41016L6 2.83016L10.59 7.41016L12 6.00016L6 0.000156403L0 6.00016L1.41 7.41016Z'
                        fill='#323232'
                      />
                    </svg>
                  </a>

                  {DropdownActive && (
                    <div className={cls.listItemDropdown}>
                      <ul>
                        {menu.slice(7).map((item) => (
                          <li key={item.id}>
                            <AnchorLink
                              offset={80}
                              href={`#${item.id}`}
                              style={{ whiteSpace: 'nowrap' }}
                              onClick={() => {
                                if (activeLink.includes(item.id)) {
                                  setActiveLink('')
                                } else {
                                  setActiveLink(String(item.id))
                                }
                              }}
                            >
                              {item.title}
                            </AnchorLink>
                          </li>
                        ))}
                      </ul>
                    </div>
                  )}
                </div>
              </div>
            </ul>
          )}
        </div>
      </Container>
    </div>
  )
}

export default Menu
