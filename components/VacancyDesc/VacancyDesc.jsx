import React, { useState } from 'react'
import { Box, Typography } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import cls from './VacancyDesc.module.scss'
import VacancyRequestForm from '../VacancyRequestDialog/VacancyRequestForm'
import { useRouter } from 'next/router'
import { btnText } from '../../mock/menu'

function VacancyDesc({ data }) {
  const [open, setOpen] = useState(false)
  const { locale } = useRouter()

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <>
      <Typography variant={`h1`} color={`black`} className={cls.title}>
        {data.title}
      </Typography>
      <Box className={cls.wrapper}>
        <CardWrapper>
          {data.requirements.map((item, index) => (
            <Box key={index} marginBottom='24px'>
              <Box marginBottom='16px'>
                <Typography variant={`h16`}>{item.title}</Typography>
              </Box>
              {item.infos.map((el, ind) => (
                <Box
                  key={el.info + ind}
                  display='flex'
                  sx={{ marginBottom: '16px' }}
                >
                  <Typography variant={`h14`} className={cls.listStyle}>
                    {el.title}
                  </Typography>
                </Box>
              ))}
            </Box>
          ))}
          <Box display='flex' marginTop='8px'>
            <button className={cls.btn}>{btnText[locale].share}</button>
            <button className={cls.btnPrimary} onClick={handleOpen}>
              {btnText[locale].apply}
            </button>
          </Box>
        </CardWrapper>
      </Box>
      <VacancyRequestForm onClose={handleClose} open={open} />
    </>
  )
}

export default VacancyDesc
