import React, { useState } from 'react'
import { Container, Grid, Typography } from '@mui/material'
import cls from './Products.module.scss'
import ProductInfo from '../ProductInfo/ProductInfo'
import ProductItem from '../ProductItem/ProductItem'

function Product({ menu }) {
  const [productItem, setProductItem] = useState(null)

  const handleOpen = (event, item) => {
    event.preventDefault()
    setProductItem(item)
  }

  const handleClose = () => {
    setProductItem(null)
  }

  return (
    <div className={cls.root} id='productRoot'>
      <Container maxWidth='xl'>
        {menu.map((product) => (
          <section key={product.id} className={cls.product} id={product.id}>
            <Typography
              variant={`h1`}
              color={`black`}
              className={cls.productTitle}
            >
              {product.title}
            </Typography>
            <Grid container rowSpacing={2} spacing={2}>
              {product.products.map((item) => (
                <Grid item xl={2} xs={6} md={3} key={item.id}>
                  <ProductItem item={item} cls={cls} handleOpen={handleOpen} />
                </Grid>
              ))}
            </Grid>
          </section>
        ))}
      </Container>

      {!!productItem && (
        <ProductInfo
          open={!!productItem}
          onClose={handleClose}
          data={productItem}
        />
      )}
    </div>
  )
}

export default Product
