import { Box, IconButton, Typography } from '@mui/material'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { btnText } from '../../mock/menu'
import {
  addToCartAction,
  reduceCartItemQuantityAction,
  removeFromCartAction,
} from '../../store/actions/cartActions/cartActions'
import { addToCartWithAnimation } from '../../utils/addToCartWithAnimation'
import { numberToPrice } from '../../utils/numberToPrice'
import { MinusIcon, PlusIcon } from '../Icons'

export default function ProductItem({ item, cls, handleOpen }) {
  const dispatch = useDispatch()
  const { cartItems } = useSelector((state) => state.cart)
  const { locale } = useRouter()
  const cartItem = cartItems && cartItems.find((data) => data.id === item.id)

  const addToCart = (event) => {
    addToCartWithAnimation(event, item, dispatch, addToCartAction)
  }

  const handleSubtract = (event) => {
    event.stopPropagation()
    event.preventDefault()
    if (cartItem.quantity !== 1) {
      dispatch(reduceCartItemQuantityAction(item))
    } else {
      dispatch(removeFromCartAction(item))
    }
  }
  const handleAdd = (event) => {
    event.stopPropagation()
    event.preventDefault()
    dispatch(addToCartAction(item))
  }
  return (
    <Link href='/product'>
      <a
        className={`${cls.productCard} cart-item`}
        style={{ position: 'relative' }}
        onClick={(event) => handleOpen(event, item)}
      >
        <Box className={cls.animationBox} id={item.id}>
          <Image
            loader={() => `${process.env.CDN}/${item.deliveryImage}`}
            src={`${process.env.CDN}/${item.deliveryImage}`}
            alt={item.title}
            width={100}
            height={75}
          />
        </Box>
        <Image
          loader={() => `${process.env.CDN}/${item.deliveryImage}`}
          src={`${process.env.CDN}/${item.deliveryImage}`}
          alt={item.title}
          width={300}
          height={218}
        />
        <div className={cls.productBody}>
          <div className={cls.productTexts}>
            <Typography variant={`h2`}>
              {numberToPrice(item.price.salePrice)}
            </Typography>
            <Box className={cls.productName}>
              <Typography
                variant={`body3`}
                height='30px'
                sx={{
                  display: 'inline-block',
                  width: '100%',
                  whiteSpace: 'nowrap',
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                }}
              >
                {item.title}
              </Typography>
            </Box>
            <Box height={36} overflow='hidden'>
              <Typography variant={`body2`}>{item.description}</Typography>
            </Box>
          </div>
          {!!cartItem ? (
            <div className={cls.productCounter}>
              <IconButton onClick={handleSubtract} className={cls.counterBtn}>
                <MinusIcon />
              </IconButton>
              <Typography variant={`h4`}>{cartItem.quantity}</Typography>
              <IconButton onClick={handleAdd} className={cls.counterBtn}>
                <PlusIcon />
              </IconButton>
            </div>
          ) : (
            <button
              className={`${cls.productButton} cart-item`}
              onClick={addToCart}
            >
              <Typography variant={`h4`}>
                {btnText[locale].addToCart}
              </Typography>
            </button>
          )}
        </div>
      </a>
    </Link>
  )
}
