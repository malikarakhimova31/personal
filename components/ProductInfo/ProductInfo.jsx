import React, { useState } from 'react'
import { Drawer, Dialog, Typography, Button } from '@mui/material'
import { makeStyles } from '@mui/styles'
import Image from 'next/image'
import CloseIconEffect from '../CloseIcon/CloseIcon'
import { numberToPrice } from '../../utils/numberToPrice'
import { Box } from '@mui/system'
import { useDispatch } from 'react-redux'
import { setToCartAction } from '../../store/actions/cartActions/cartActions'
import ProductCounter from '../ProductCounter/ProductCounter'
import { addToCartWithAnimation } from '../../utils/addToCartWithAnimation'
import { useIsMobile } from '../../utils/useIsMobile'

const useStyles = makeStyles(() => ({
  paper: {
    borderRadius: '0',
  },
}))

function ProductInfo({ onClose, open, data }) {
  const [isMobile] = useIsMobile()
  const dispatch = useDispatch()
  const [count, setCount] = useState(1)
  const classes = useStyles()
  const addProductToCard = (event) => {
    const item = {
      ...data,
      quantity: count,
    }
    addToCartWithAnimation(event, item, dispatch, setToCartAction)
    onClose()
  }
  return (
    <>
      <Dialog
        onClose={onClose}
        open={!isMobile && open}
        classes={{ paper: classes.paper }}
        BackdropProps={{ style: { backgroundColor: 'rgba(0, 0, 0, 0.8)' } }}
      >
        <div id='productDialog'>
          <CloseIconEffect onClick={onClose} />
          <Box mx='auto' mt={-4}>
            <Image
              loader={() => `${process.env.CDN}/${data.deliveryImage}`}
              src={`${process.env.CDN}/${data.deliveryImage}`}
              alt={data.title}
              width={250}
              height={200}
            />
          </Box>
          <div className='card'>
            <Typography variant={`h1`}>{data.title}</Typography>
            <Typography variant={`body1`} sx={{ maxWidth: 490 }}>
              {data.description}
            </Typography>
          </div>
          <div className='bottomBox'>
            <ProductCounter
              count={count}
              setCount={setCount}
              data={data}
              style={{ marginRight: '32px' }}
            />
            <Button className='formButton' onClick={addProductToCard}>
              <Typography variant={`h5`} sx={{ mr: 8 }} color='inherit'>
                Добавить в корзину
              </Typography>
              <Typography variant={`h5`} color='inherit'>
                {numberToPrice(data.price.salePrice)}
              </Typography>
            </Button>
          </div>
        </div>
      </Dialog>
      <Drawer
        anchor='bottom'
        open={isMobile && open}
        onClose={onClose}
        id='bottomDrawerBox'
        BackdropProps={{ style: { backgroundColor: 'rgba(0, 0, 0, 0.8)' } }}
      >
        <div id='productDialog'>
          <CloseIconEffect onClick={onClose} />
          <Box mx='auto' mt={-4}>
            <Image
              loader={() => `${process.env.CDN}/${data.deliveryImage}`}
              src={`${process.env.CDN}/${data.deliveryImage}`}
              alt={data.title}
              width={250}
              height={200}
            />
          </Box>
          <div className='card'>
            <Typography variant={`h1`}>{data.title}</Typography>
            <Typography variant={`body1`} sx={{ maxWidth: 490 }}>
              {data.description}
            </Typography>
          </div>
          <div className='bottomBox'>
            <ProductCounter
              count={count}
              setCount={setCount}
              data={data}
              style={{ marginRight: '10px' }}
            />
            <Button
              className='formButton'
              onClick={addProductToCard}
              style={{ borderRadius: '0 !important' }}
            >
              <Typography variant={`h22`} sx={{ mr: 0, whiteSpace: 'wrap' }}>
                Добавить в корзину
              </Typography>
            </Button>
          </div>
        </div>
      </Drawer>
    </>
  )
}

export default ProductInfo
