import React, { useEffect, useState } from 'react'
import { Box, Typography } from '@mui/material'
import cls from './DeliveryAddress.module.scss'
import {
  GeolocationControl,
  Map,
  Placemark,
  SearchControl,
  YMaps,
} from 'react-yandex-maps'
import CardWrapper from '../CardWrapper/CardWrapper'
import Input from '../Input/Input'
import { getAddressFromMap } from '../../utils/getAddressFromMap'
import axios from '../../utils/axios'
import { menu } from '../../mock/menu'
import { useRouter } from 'next/router'

const defaultCoords = {
  lat: '41.322678',
  long: '69.191592',
  address: 'Evos',
}

export default function DeliveryAddress({ formik }) {
  const [data, setData] = useState(defaultCoords)
  const [address, setAddress] = useState('')
  const { locale } = useRouter()

  function getGeoLocation() {
    navigator.geolocation.getCurrentPosition(
      function (position) {
        setData({
          ...data,
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        })
      },
      function (error) {
        getAddressFromMap(setAddress, data)
        console.log('error => ', error)
      }
    )
  }

  useEffect(() => {
    getGeoLocation()
  }, [])

  useEffect(() => {
    const coords = [data.lat, data.long].join(',')
    formik.setFieldValue('address', address)
    formik.setFieldValue('geoLocation', coords)
  }, [address])

  useEffect(() => {
    getAddressFromMap(setAddress, data)
    getNearestBranch(data.lat, data.long)
  }, [data])

  function getNearestBranch(lat, long, companyId = '1') {
    const coords = [lat, long].join(',')
    axios
      .get(`/branch/nearby/${coords}/${companyId}`)
      .then((res) => {
        const branch = res.branches[0]
        getBranchById(branch?.id)
        formik.setFieldValue('branchId', branch?.id)
      })
      .catch((err) => console.log(err))
  }

  function getBranchById(branchId) {
    axios
      .get(`/menu/bracnh/${branchId}`)
      .then((res) => {
        const deliveryDetails = res.deliveryDetails[0]
        console.log('deliveryDetails => ', deliveryDetails)
        formik.setFieldValue('deliveryDetails', deliveryDetails)
      })
      .catch((err) => console.log(err))
  }

  return (
    <CardWrapper mt={2}>
      <Typography variant={`h1`} color={`black`}>
        {menu[locale].deliveryAddress}
      </Typography>
      {/* <Box className={cls.inputWrapper} mt={4}>
        <Input
          name='address'
          placeholder='Кв./офис'
          className={cls.personalInput}
        />
        <Input
          name='domofon'
          placeholder='Домофон'
          className={cls.personalInput}
        />
        <Input
          name='podyezd'
          placeholder='Подъезд'
          className={cls.personalInput}
        />
        <Input name='etaj' placeholder='Этаж' className={cls.personalInput}/>
      </Box> */}
      <Box mt={2}>
        <Input
          name='address'
          placeholder='Адрес'
          value={address}
          className={cls.personalInput}
        />
      </Box>
      <Box mt={2}>
        <YMaps
          query={{
            apikey: process.env.MAP_API_KEY,
          }}
        >
          <Map
            style={{ height: '300px' }}
            onClick={(e) => {
              console.log('e => ', e)
              setData({
                ...data,
                lat: e.get('coords')[0],
                long: e.get('coords')[1],
              })
            }}
            onChange={(e) => {
              console.log('e => ', e)
              setData({
                ...data,
                lat: e.get('coords')[0],
                long: e.get('coords')[1],
              })
            }}
            state={{
              center: [data.lat, data.long],
              zoom: 16,
              controls: ['zoomControl', 'fullscreenControl'],
            }}
            modules={['control.ZoomControl', 'control.FullscreenControl']}
          >
            <GeolocationControl options={{ float: 'left' }} />
            <SearchControl options={{ placeholderContent: data.address }} />
            <Placemark
              options={{ iconColor: '#000' }}
              geometry={[data.lat, data.long]}
            />
          </Map>
        </YMaps>
      </Box>
    </CardWrapper>
  )
}
