import React from 'react'
import { Box, Typography } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import Image from 'next/image'
import cls from './BranchesItems.module.scss'
import { ArrowIcon } from '../Icons'
import Link from 'next/link'

export default function BranchItems({ active, branches }) {
  return (
    <div className={` ${cls.root} ${active && cls.active}`}>
      {branches.branches.map((el, ind) => (
        <Box
          display='flex'
          key={ind}
          justifyContent='space-between'
          className={cls.marginVertical}
        >
          <Box style={{ cursor: 'pointer' }} className={cls.boxWidth}>
            <Link href={`/branches/${el.id}`} passHref>
              <div className={cls.box}>
                <CardWrapper>
                  <Box display='flex'>
                    <Box display='flex' className={cls.branchBox}>
                      <div className={cls.imageBox}>
                        <img
                          src='/images/svg/customBranchImage.svg'
                          alt='branch'
                        />
                      </div>
                      <Box
                        display='flex'
                        flexDirection='column'
                        flexWrap='nowrap'
                        sx={{
                          marginLeft: 3,
                          '@media (max-width: 576px)': { marginLeft: 0 },
                        }}
                      >
                        <Typography
                          variant={`h6`}
                          color={`black`}
                          sx={{ marginBottom: '8px' }}
                        >
                          {el.title}
                        </Typography>
                        <Typography variant={`h14`} sx={{ maxWidth: '250px' }}>
                          {el.address}
                        </Typography>
                      </Box>
                    </Box>
                    <Box className={cls.workingHoursBox} flexBasis='30%'>
                      <Box
                        display='flex'
                        mt={2}
                        flexDirection='column'
                        alignItems='flex-start'
                        justifyContent='flex-start'
                        marginLeft='16px'
                      >
                        <Typography
                          variant={`h15`}
                          sx={{ marginBottom: '8px' }}
                        >
                          Время работы
                        </Typography>
                        <Typography>9:00 – 23:00</Typography>
                      </Box>
                    </Box>
                    <div className={cls.icon}>
                      <ArrowIcon />
                    </div>
                  </Box>
                </CardWrapper>
              </div>
            </Link>
          </Box>
        </Box>
      ))}
    </div>
  )
}
