import React from 'react'
import { CircularProgress, Typography } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import cls from './CheckoutInfo.module.scss'
import { Box } from '@mui/system'
import { shallowEqual, useSelector } from 'react-redux'
import { cartTotalPriceSelector } from '../../store/selectors/cartSelectors'
import { numberToPrice } from '../../utils/numberToPrice'
import { menu } from '../../mock/menu'
import { useRouter } from 'next/router'

export default function CheckoutInfo({ loading, deliveryPrice = 9000 }) {
  const totalPrice = useSelector(
    (state) => cartTotalPriceSelector(state),
    shallowEqual
  )

  const { locale } = useRouter()

  return (
    <div className={cls.root}>
      <CardWrapper>
        <Typography variant={`h1`} color={`black`}>
          {menu[locale].total}
        </Typography>
        <Box mt={4} pb={2} boxShadow='inset 0px -1px 0px rgba(0, 0, 0, 0.08);'>
          <Box
            display='flex'
            justifyContent='space-between'
            alignItems='center'
            mb={1}
          >
            <Typography>{menu[locale].priceOfOrder}</Typography>
            <Typography color='black'>{numberToPrice(totalPrice)}</Typography>
          </Box>
          <Box
            display='flex'
            justifyContent='space-between'
            alignItems='center'
          >
            <Typography>{menu[locale].priceOfDelivery}</Typography>
            <Typography color='black'>
              {numberToPrice(deliveryPrice)}
            </Typography>
          </Box>
        </Box>
        <Box
          display='flex'
          justifyContent='space-between'
          alignItems='center'
          mt={2}
        >
          <Typography variant='caption' color='black'>
            {menu[locale].totalPrice}
          </Typography>
          <Typography variant='caption' color='primary'>
            {numberToPrice(totalPrice + deliveryPrice)}
          </Typography>
        </Box>
        <Box mt={4}>
          <button type='submit' className={cls.button}>
            {loading ? (
              <CircularProgress color='inherit' size={18} />
            ) : (
              menu[locale].accept
            )}
          </button>
        </Box>
      </CardWrapper>
    </div>
  )
}
