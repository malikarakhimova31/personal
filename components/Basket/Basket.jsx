import React, { useState } from 'react'
import { Box, Drawer, IconButton, Typography, Divider } from '@mui/material'
import { useRouter } from 'next/router'
import { TrashIcon } from '../Icons'
import Image from 'next/image'
import Counter from '../Counter/Counter'
import cls from './Basket.module.scss'
import CloseIconEffect from '../CloseIcon/CloseIcon'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { numberToPrice } from '../../utils/numberToPrice'
import { cartTotalPriceSelector } from '../../store/selectors/cartSelectors'
import {
  clearCartAction,
  removeFromCartAction,
} from '../../store/actions/cartActions/cartActions'
import { closeCartDrawer } from '../../store/actions/drawerActions/drawerActions'
import { LoupeIcon } from '../Icons'
import FullButton from '../FullButton/FullButton'
import { menu } from '../../mock/menu'
import ConfirmationPopup from '../ConfirmationPopup/ConfirmationPopup'
import EmptyBasket from './EmptyBasket'

function Basket() {
  const { cartItems } = useSelector((state) => state.cart, shallowEqual)
  const totalPrice = useSelector(
    (state) => cartTotalPriceSelector(state),
    shallowEqual
  )
  const { isCartOpen } = useSelector((state) => state.drawer, shallowEqual)
  const dispatch = useDispatch()
  const { locale, push } = useRouter()
  const [isOpenPopup, setIsOpenPopup] = useState(false)

  const closeDrawer = () => {
    dispatch(closeCartDrawer())
  }

  const removeItemFromCart = (item) => {
    if (cartItems.length === 1) {
      closeDrawer()
    }
    dispatch(removeFromCartAction(item))
  }

  const clearCart = () => {
    dispatch(clearCartAction())
    closeDrawer()
    handleClosePopup()
  }

  const handleClosePopup = () => {
    setIsOpenPopup(false)
  }

  const handleOpenPopup = () => {
    setIsOpenPopup(true)
  }

  return (
    <div className={cls.root}>
      <Drawer
        anchor='right'
        open={isCartOpen}
        onClose={closeDrawer}
        id='shopBox'
        BackdropProps={{ style: { backgroundColor: 'rgba(0, 0, 0, 0.8)' } }}
      >
        {cartItems.length > 0 ? (
          <>
            <CloseIconEffect
              onClick={closeDrawer}
              style={{
                marginTop: '0',
                marginRight: '0',
                marginLeft: '0',
                marginBottom: '12px',
              }}
            />
            <div className='header'>
              <Typography variant={`h6`}>{menu[locale].yourOrder}</Typography>
              <Typography
                variant={`h7`}
                sx={{ cursor: 'pointer' }}
                onClick={handleOpenPopup}
              >
                {menu[locale].clearBasket}
              </Typography>
            </div>
            <Divider />
            <div className='all'>
              <div className='basketBody'>
                {cartItems.length > 0
                  ? cartItems.map((item) => (
                      <div key={item.id} className='items'>
                        <div className='right'>
                          <IconButton
                            onClick={() => removeItemFromCart(item)}
                            className={cls.iconButton}
                            sx={{ padding: '0' }}
                          >
                            <TrashIcon />
                          </IconButton>
                          <Box
                            width={96}
                            height={96}
                            ml={2}
                            className={cls.countBadge}
                          >
                            <Image
                              loader={() =>
                                `${process.env.CDN}/${item.deliveryImage}`
                              }
                              src={`${process.env.CDN}/${item.deliveryImage}`}
                              alt={item.title}
                              width={96}
                              height={96}
                            />
                          </Box>
                          <div className='title'>
                            <Typography
                              sx={{ marginBottom: 1 }}
                              className='prodTitle'
                            >
                              {item.title}
                            </Typography>
                            <Typography
                              className={cls.description}
                              variant={`body4`}
                              sx={{
                                maxWidth: '178px',
                                height: 40,
                                overflow: 'hidden',
                              }}
                            >
                              {item.description}
                            </Typography>
                          </div>
                        </div>
                        <Box
                          display='flex'
                          flexDirection='column'
                          justifyContent='center'
                        >
                          <Typography variant={`h8`} className={cls.price}>
                            {numberToPrice(
                              item.price.salePrice * item.quantity
                            )}
                          </Typography>
                          <Counter data={item} />
                        </Box>
                      </div>
                    ))
                  : ''}
              </div>
              <Box className={cls.cartActions}>
                <div className='total'>
                  <Typography className='totalTitle'>
                    {menu[locale].total}
                  </Typography>
                  <Typography className='totalPrice'>
                    {numberToPrice(totalPrice)}
                  </Typography>
                </div>
                <FullButton
                  text={menu[locale].setOrder}
                  onClick={() => {
                    push('/checkout')
                    closeDrawer()
                  }}
                />
              </Box>
            </div>
          </>
        ) : (
          <EmptyBasket closeDrawer={closeDrawer} />
        )}
      </Drawer>
      <ConfirmationPopup
        open={isOpenPopup}
        onClose={handleClosePopup}
        clearCart={clearCart}
      />
    </div>
  )
}

export default Basket
