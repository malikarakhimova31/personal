import React from 'react'
import { Box } from '@mui/material'
import { YMaps, Map, ZoomControl, GeoObject } from 'react-yandex-maps'

const mapState = {
  center: [41.31, 69.31],
  zoom: 12,
}

export default function BranchMap({ branches }) {
  return (
    <Box
      sx={{
        background: '#fff',
        padding: '32px',
        marginTop: '24px',
        minHeight: '70vh',
      }}
    >
      <YMaps>
        <Map
          width='100%'
          height='100vh'
          state={mapState}
          instanceRef={(ref) => {
            ref && ref.behaviors.disable('scrollZoom')
          }}
        >
          {branches.branches.map((branch, i) => (
            <GeoObject
              key={i}
              options={{ iconColor: '#f5363e' }}
              geometry={{
                type: 'Point',
                coordinates: [
                  parseFloat(branch.geoLocation.split(',')[0]),
                  parseFloat(branch.geoLocation.split(',')[1]),
                ],
              }}
              properties={{ hintContent: branch.title }}
              modules={['geoObject.addon.hint']}
            />
          ))}
          <ZoomControl
            options={{
              size: 'auto',
              zoomDuration: 500,
            }}
          />
        </Map>
      </YMaps>
    </Box>
  )
}
