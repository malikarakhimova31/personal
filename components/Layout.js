import React from 'react'
import dynamic from 'next/dynamic'
import Header from './Header/Header'

const Footer = dynamic(() => import('./Footer/Footer'))
const Notification = dynamic(() => import('./Notification/Notification'))
const Basket = dynamic(() => import('./Basket/Basket'))

function Layout({ children }) {
  return (
    <>
      <Header />
      {children}
      <Footer />

      <Notification />
      <Basket />
    </>
  )
}

export default Layout
